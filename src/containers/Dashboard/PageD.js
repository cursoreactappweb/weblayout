import React from "react";
import { Link } from "react-router-dom";

//@components
import { Modal } from "../../components";

export default () => {
  return (
    <div>
      <Link to="/layout">Ir a Layout</Link>
      <Modal />
    </div>
  );
};
