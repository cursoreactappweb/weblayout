// @vendors
import React from "react";
import { Link, Switch, Route } from "react-router-dom";
import Tab from "../../components/Tab";
import Countries from "../countries";
import PageD from "./PageD";

export default () => {
  return (
    <div className="dashboard">
      <nav className="dashboard-nav">
        <ul>
          <li>
            <Link to="/dashboard">Dashboard</Link>
          </li>
          <li>
            <Link to="/dashboard/page-c">Page C</Link>
          </li>
          <li>
            <Link to="/dashboard/page-d">Page D</Link>
          </li>
        </ul>
      </nav>
      <div className="dashboard-content">
        <Switch>
          <Route
            exact
            path="/dashboard"
            component={() => (
              <Tab defaultTab="tab1">
                <Tab.TabItem title="Tab 1" id="tab1">
                  tab 1
                </Tab.TabItem>
                <Tab.TabItem title="Tab 2" id="tab2">
                  tab 2
                </Tab.TabItem>
                <Tab.TabItem title="Tab 3" id="tab3">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Maecenas cursus iaculis fermentum. Sed auctor, arcu id
                    venenatis gravida, lacus odio hendrerit nisi, nec euismod
                    justo neque et velit. Nullam eleifend diam vestibulum nibh
                    congue luctus. Mauris sagittis convallis nisl in malesuada.
                    Ut dui arcu, pharetra sit amet blandit sit amet, euismod
                    vestibulum risus. Duis lobortis, felis cursus cursus
                    volutpat, felis turpis commodo risus, ac euismod neque leo
                    vel nibh. Duis cursus lectus eu velit suscipit dictum. Nunc
                    in hendrerit justo.
                  </p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Maecenas cursus iaculis fermentum. Sed auctor, arcu id
                    venenatis gravida, lacus odio hendrerit nisi, nec euismod
                    justo neque et velit. Nullam eleifend diam vestibulum nibh
                    congue luctus. Mauris sagittis convallis nisl in malesuada.
                    Ut dui arcu, pharetra sit amet blandit sit amet, euismod
                    vestibulum risus. Duis lobortis, felis cursus cursus
                    volutpat, felis turpis commodo risus, ac euismod neque leo
                    vel nibh. Duis cursus lectus eu velit suscipit dictum. Nunc
                    in hendrerit justo.
                  </p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Maecenas cursus iaculis fermentum. Sed auctor, arcu id
                    venenatis gravida, lacus odio hendrerit nisi, nec euismod
                    justo neque et velit. Nullam eleifend diam vestibulum nibh
                    congue luctus. Mauris sagittis convallis nisl in malesuada.
                    Ut dui arcu, pharetra sit amet blandit sit amet, euismod
                    vestibulum risus. Duis lobortis, felis cursus cursus
                    volutpat, felis turpis commodo risus, ac euismod neque leo
                    vel nibh. Duis cursus lectus eu velit suscipit dictum. Nunc
                    in hendrerit justo.
                  </p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Maecenas cursus iaculis fermentum. Sed auctor, arcu id
                    venenatis gravida, lacus odio hendrerit nisi, nec euismod
                    justo neque et velit. Nullam eleifend diam vestibulum nibh
                    congue luctus. Mauris sagittis convallis nisl in malesuada.
                    Ut dui arcu, pharetra sit amet blandit sit amet, euismod
                    vestibulum risus. Duis lobortis, felis cursus cursus
                    volutpat, felis turpis commodo risus, ac euismod neque leo
                    vel nibh. Duis cursus lectus eu velit suscipit dictum. Nunc
                    in hendrerit justo.
                  </p>
                </Tab.TabItem>
              </Tab>
            )}
          />
          <Route path="/dashboard/page-c" component={() => <Countries />} />
          <Route path="/dashboard/page-d" component={() => <PageD />} />
        </Switch>
      </div>
    </div>
  );
};
