import React, { useEffect, useState } from "react";
import Select from "react-select";
import axios from "axios";

const API = "https://restcountries.eu/rest/v2";

export default () => {
  const [countries, setCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState();

  //()=>{} ->callback
  useEffect(() => {
    //se va a ejecutar al montar el componente
    const getCountries = async () => {
      const res = await axios.get(`${API}/all`);
      //console.log(res.data);
      const data = res.data.map(item => ({
        label: item.name,
        value: item.alpha2Code
      }));
      setCountries(data);
    };
    getCountries();
  }, []); //si al menos cambia alguna de las variables en el arreglo definido,
  //se ejecutara el contenido dentro de la funcion callback de useEffect.
  //es decir, cambiara cada vez que cambie la variable del array
  //sino se pone nada en el arreglo solo se ejecutara una vez cuando se monte

  const handleChange = async selectedOption => {
    const res = await axios.get(`${API}/alpha/${selectedOption.value}`);
    setSelectedCountry({
      ...selectedCountry,
      flag: res.data.flag
    });
  };

  return (
    <div>
      <Select options={countries} onChange={handleChange} />
      {selectedCountry && (
        <div>
          <img src={selectedCountry.flag} height="200" />
        </div>
      )}
    </div>
  );
};
