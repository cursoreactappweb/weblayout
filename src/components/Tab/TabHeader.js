import React, { useContext } from "react";
import { TabContext } from "./index";
import classnames from "classnames";
import styled from "styled-components";
//@styles
import { StyledUl, StyledButton, StyledLi } from "./styles"; //cuando no lleva export default se declara asi

const TabHeaderButton = ({ onSelectTab, child }) => {
  const tabContext = useContext(TabContext);

  const btnClass = classnames({
    selected: child.props.id === tabContext.selectedTab
  });

  return (
    <StyledLi className={btnClass}>
      <StyledButton
        className={btnClass}
        onClick={() => tabContext.onSelectTab(child.props.id)}
      >
        {child.props.title}
      </StyledButton>
    </StyledLi>
  );
};

const TabHeader = ({ tabs }) => {
  return (
    <StyledUl>
      {React.Children.map(tabs, child => (
        <TabHeaderButton child={child} />
      ))}
    </StyledUl>
  );
};

export default TabHeader;
